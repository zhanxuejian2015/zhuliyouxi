class ShareScene extends egret.DisplayObjectContainer {

    public main:Main;
    public title:string;
    public constructor(main:Main,title:string) {
        super();
        this.main = main;
        this.title=title;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    public onAddToStage():void {
        document.title=this.title;
        window.location.href=this.changeURLArg("tid",this.main.uid);
        var sky:egret.Bitmap = this.createBitmapByName("common.fx");
        this.addChild(sky);
        var stageW:number = this.stage.stageWidth;
        var stageH:number = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;
        sky.touchEnabled=true;
        sky.addEventListener(egret.TouchEvent.TOUCH_TAP,this.skyTouch,this);

    }
    private  skyTouch():void{

    }

    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name:string):egret.Bitmap {
        var result:egret.Bitmap = new egret.Bitmap();
        var texture:egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

    public changeURLArg(arg:string,val:string):string{
        var url=window.location.href;
        var pattern = arg + '=([^&]*)';
        var replaceText = arg + '=' + val;
        if (url.match(pattern)) {
            var tmp = '/(' + arg + '=)([^&]*)/gi';
            tmp = url.replace(eval(tmp), replaceText);
            return tmp;
        } else {
            if (url.match('[\?]')) {
                return url + '&' + replaceText;
            } else {
                return url + '?' + replaceText;
            }
        }
    }

}