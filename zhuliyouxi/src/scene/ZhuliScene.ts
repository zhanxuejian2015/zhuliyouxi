/**
 * Created by bannika on 15/6/16.
 */

class ZhuliScene extends egret.DisplayObjectContainer {
    public main:Main;
    private maxCount:number = 300;
    private count:number = 0;

    private jdt:egret.Bitmap;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.count = main.score;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createScene, this);
    }


    /**
     * 创建场景界面
     * Create scene interface
     */
    private createScene():void {

        var sky:egret.Bitmap = this.createBitmapByName("common.bg");
        this.addChild(sky);
        var stageW:number = this.stage.stageWidth;
        var stageH:number = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;

        var bx:egret.Bitmap = this.createBitmapByName("common.bx");
        this.addChild(bx);
        bx.x = stageW / 2 - bx.width / 2;
        bx.y = stageH / 2 - bx.height / 2;

        var btzl:CommonButton = new CommonButton("common.btzl");
        this.addChild(btzl);
        btzl.addEventListener(egret.TouchEvent.TOUCH_TAP, this.zhuli, this);

        var jd:egret.Bitmap = this.createBitmapByName("common.jdt");
        this.addChild(jd);
        jd.x = stageW / 2 - jd.width / 2;
        jd.y = bx.y + bx.height + 20;

        this.jdt = this.createBitmapByName("common.jd");
        this.addChild(this.jdt);
        this.jdt.x = jd.x + 10;
        this.jdt.y = jd.y + 10;
        this.jdt.width = this.count / this.maxCount * 400;
        var zjyw:CommonButton = new CommonButton("common.wyyw");
        this.addChild(zjyw);
        zjyw.addEventListener(egret.TouchEvent.TOUCH_TAP, this.zjyw, this);
        btzl.y = zjyw.y = 630;
        btzl.x = bx.x;
        zjyw.x = bx.x + bx.width - zjyw.width;


    }


    /**
     * 为他助力
     */
    private zhuli():void {
        this.count++;
        //调用存储接口
        this.jdt.width = this.count / this.maxCount * 400;
        if(this.count>=this.maxCount){
            this.count=this.maxCount;
        }
        var url:string = this.main.cmdurl+"/zhuli/"+this.main.uid;
        var loader:egret.URLLoader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.VARIABLES;
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);
        var request:egret.URLRequest = new egret.URLRequest(url);
        request.method = egret.URLRequestMethod.POST;
        request.data = new egret.URLVariables("activityid"+this.main.appid);
        loader.load(request);
    }
    private onPostComplete(event:egret.Event):void {
        var loader:egret.URLLoader = <egret.URLLoader> event.target;
        var data:egret.URLVariables = loader.data;
        var json=JSON.parse(data.toString());
        if(json.code==2001){
           //成功
        }

    }
    /**
     * 我也要玩
     */
    private zjyw():void {
        //更换url的tid=uid
        this.main.tid=this.main.uid;
        var share:ShareScene = new ShareScene(this.main,"大家快来帮我助力赢大奖");
        this.removeChildren();
        this.addChild(share);
    }


    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name:string):egret.Bitmap {
        var result:egret.Bitmap = new egret.Bitmap();
        var texture:egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }


    private  reset():void {

    }


    /**
     *游戏结束
     */

    private gameover():void {
        //this.main.score = this.score;

        //var resultScene:ResultScene = new ResultScene(this.main);
        //this.main.removeChildren();
        //this.main.addChild(resultScene);

    }


}