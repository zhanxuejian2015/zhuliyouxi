class AwardScene extends egret.DisplayObjectContainer {
    private function

    public main:Main;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    public onAddToStage():void {
        var sky:egret.Bitmap = this.createBitmapByName("common.bg");
        this.addChild(sky);
        var stageW:number = this.stage.stageWidth;
        var stageH:number = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;

        var jlpanel:egret.Bitmap = this.createBitmapByName("common.panel");
        this.addChild(jlpanel);
        jlpanel.x=stageW/2-jlpanel.width/2;
        jlpanel.y=stageH/2-jlpanel.height/2;

        var yhq:egret.Bitmap = this.createBitmapByName("common.yhq");
        this.addChild(yhq);
        yhq.x=stageW/2-yhq.width/2-5;
        yhq.y=stageH/2-yhq.height/2;

        var lqjl:CommonButton = new CommonButton("common.lqjl");
        this.addChild(lqjl);
        lqjl.addEventListener(egret.TouchEvent.TOUCH_TAP,this.lqjlHandle,this);

        var zstr:CommonButton = new CommonButton("common.zspy");
        this.addChild(zstr);
        zstr.addEventListener(egret.TouchEvent.TOUCH_TAP,this.zstrHandle,this);
        lqjl.y=zstr.y=520;
        lqjl.x=jlpanel.x+23;
        zstr.x=lqjl.x+lqjl.width+10;

    }

    private lqjlHandle():void{
        //领取奖励
        console.log("领取奖励！！！");
        var url:string = this.main.cmdurl+"/check/"+this.main.appid;
        var loader:egret.URLLoader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.VARIABLES;
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);
        var request:egret.URLRequest = new egret.URLRequest(url);
        request.method = egret.URLRequestMethod.POST;
       request.data = new egret.URLVariables("userId"+this.main.uid);
        loader.load(request);

    }
    private onPostComplete(event:egret.Event):void {
        var loader:egret.URLLoader = <egret.URLLoader> event.target;
        var data:egret.URLVariables = loader.data;
        var json=JSON.parse(data.toString());
        if(json.code==2001){
            //成功
        }
    }


    private zstrHandle():void{
        //分享给其他人
        var share:ShareScene = new ShareScene(this.main,"小伙伴，快来帮我拿奖品~");
        this.removeChildren();
        this.addChild(share);
    }

    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name:string):egret.Bitmap {
        var result:egret.Bitmap = new egret.Bitmap();
        var texture:egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

}