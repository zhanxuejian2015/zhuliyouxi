/**
 * Created by bannika on 15/5/27.
 */
var CommonButton = (function (_super) {
    __extends(CommonButton, _super);
    function CommonButton(texture) {
        _super.call(this);
        this.texture = "common.btyb";
        this.texture = texture;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createView, this);
    }
    var __egretProto__ = CommonButton.prototype;
    __egretProto__.createView = function () {
        var img = new egret.Bitmap();
        img.texture = RES.getRes(this.texture);
        var labelTF = new egret.TextField();
        labelTF.text = this.label;
        labelTF.x = img.width / 2 - labelTF.width / 2;
        labelTF.y = img.height / 2 - labelTF.height / 2;
        this.addChild(img);
        this.addChild(labelTF);
        this.touchEnabled = true;
    };
    return CommonButton;
})(egret.DisplayObjectContainer);
CommonButton.prototype.__class__ = "CommonButton";
