var AwardScene = (function (_super) {
    __extends(AwardScene, _super);
    function AwardScene(main) {
        _super.call(this);
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    var __egretProto__ = AwardScene.prototype;
    __egretProto__.onAddToStage = function () {
        var sky = this.createBitmapByName("common.bg");
        this.addChild(sky);
        var stageW = this.stage.stageWidth;
        var stageH = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;
        var jlpanel = this.createBitmapByName("common.panel");
        this.addChild(jlpanel);
        jlpanel.x = stageW / 2 - jlpanel.width / 2;
        jlpanel.y = stageH / 2 - jlpanel.height / 2;
        var yhq = this.createBitmapByName("common.yhq");
        this.addChild(yhq);
        yhq.x = stageW / 2 - yhq.width / 2 - 5;
        yhq.y = stageH / 2 - yhq.height / 2;
        var lqjl = new CommonButton("common.lqjl");
        this.addChild(lqjl);
        lqjl.addEventListener(egret.TouchEvent.TOUCH_TAP, this.lqjlHandle, this);
        var zstr = new CommonButton("common.zspy");
        this.addChild(zstr);
        zstr.addEventListener(egret.TouchEvent.TOUCH_TAP, this.zstrHandle, this);
        lqjl.y = zstr.y = 520;
        lqjl.x = jlpanel.x + 23;
        zstr.x = lqjl.x + lqjl.width + 10;
    };
    __egretProto__.lqjlHandle = function () {
        //领取奖励
        console.log("领取奖励！！！");
    };
    __egretProto__.zstrHandle = function () {
        //分享给其他人
        var share = new ShareScene(this.main, "送给你们一个礼物咯，快来看看");
        this.removeChildren();
        this.addChild(share);
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    __egretProto__.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    return AwardScene;
})(egret.DisplayObjectContainer);
AwardScene.prototype.__class__ = "AwardScene";
