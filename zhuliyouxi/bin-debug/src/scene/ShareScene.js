var ShareScene = (function (_super) {
    __extends(ShareScene, _super);
    function ShareScene(main, title) {
        _super.call(this);
        this.main = main;
        this.title = title;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    var __egretProto__ = ShareScene.prototype;
    __egretProto__.onAddToStage = function () {
        document.title = this.title;
        var sky = this.createBitmapByName("common.fx");
        this.addChild(sky);
        var stageW = this.stage.stageWidth;
        var stageH = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;
        sky.touchEnabled = true;
        sky.addEventListener(egret.TouchEvent.TOUCH_TAP, this.skyTouch, this);
    };
    __egretProto__.skyTouch = function () {
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    __egretProto__.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    return ShareScene;
})(egret.DisplayObjectContainer);
ShareScene.prototype.__class__ = "ShareScene";
