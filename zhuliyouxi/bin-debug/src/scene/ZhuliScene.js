/**
 * Created by bannika on 15/6/16.
 */
var ZhuliScene = (function (_super) {
    __extends(ZhuliScene, _super);
    function ZhuliScene(main) {
        _super.call(this);
        this.maxCount = 300;
        this.count = 0;
        this.main = main;
        this.count = main.score;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createScene, this);
    }
    var __egretProto__ = ZhuliScene.prototype;
    /**
     * 创建场景界面
     * Create scene interface
     */
    __egretProto__.createScene = function () {
        var sky = this.createBitmapByName("common.bg");
        this.addChild(sky);
        var stageW = this.stage.stageWidth;
        var stageH = this.stage.stageHeight;
        sky.width = stageW;
        sky.height = stageH;
        var bx = this.createBitmapByName("common.bx");
        this.addChild(bx);
        bx.x = stageW / 2 - bx.width / 2;
        bx.y = stageH / 2 - bx.height / 2;
        var btzl = new CommonButton("common.btzl");
        this.addChild(btzl);
        btzl.addEventListener(egret.TouchEvent.TOUCH_TAP, this.zhuli, this);
        var jd = this.createBitmapByName("common.jdt");
        this.addChild(jd);
        jd.x = stageW / 2 - jd.width / 2;
        jd.y = bx.y + bx.height + 20;
        this.jdt = this.createBitmapByName("common.jd");
        this.addChild(this.jdt);
        this.jdt.x = jd.x + 10;
        this.jdt.y = jd.y + 10;
        this.jdt.width = this.count / this.maxCount * 400;
        var zjyw = new CommonButton("common.wyyw");
        this.addChild(zjyw);
        zjyw.addEventListener(egret.TouchEvent.TOUCH_TAP, this.zjyw, this);
        btzl.y = zjyw.y = 630;
        btzl.x = bx.x;
        zjyw.x = bx.x + bx.width - zjyw.width;
    };
    /**
     * 为他助力
     */
    __egretProto__.zhuli = function () {
        this.count++;
        //调用存储接口
        this.jdt.width = this.count / this.maxCount * 400;
        if (this.count >= this.maxCount) {
            this.count = this.maxCount;
        }
    };
    /**
     * 我也要玩
     */
    __egretProto__.zjyw = function () {
        var share = new ShareScene(this.main, "大家快来帮我助力赢大奖");
        this.removeChildren();
        this.addChild(share);
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    __egretProto__.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    __egretProto__.reset = function () {
    };
    /**
     *游戏结束
     */
    __egretProto__.gameover = function () {
        //this.main.score = this.score;
        //var resultScene:ResultScene = new ResultScene(this.main);
        //this.main.removeChildren();
        //this.main.addChild(resultScene);
    };
    return ZhuliScene;
})(egret.DisplayObjectContainer);
ZhuliScene.prototype.__class__ = "ZhuliScene";
